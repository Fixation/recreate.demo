﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recreate.Demo.Pages
{
    [Authorize(Roles = "Test.Page1,Admin")]
    public class Page1Model : PageModel
    {
        private readonly ILogger<Page1Model> _logger;

        public Page1Model(ILogger<Page1Model> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }
    }
}
