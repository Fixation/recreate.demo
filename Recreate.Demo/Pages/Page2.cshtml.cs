﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recreate.Demo.Pages
{
    [Authorize(Roles = "Test.Page2,Admin")]
    public class Page2Model : PageModel
    {
        private readonly ILogger<Page2Model> _logger;

        public Page2Model(ILogger<Page2Model> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }
    }
}
