﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Recreate.Demo.Data;
using Recreate.Demo.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recreate.Demo.ApiControllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly ILogger<RolesController> logger;
        private readonly ApplicationDbContext context;
        private readonly UserManager<IdentityUser> userManager;

        public RolesController(ILogger<RolesController> logger, ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            this.logger = logger;
            this.context = context;
            this.userManager = userManager;
        }

        [HttpPut]
        public async Task<Role> CreateAsync(string name)
        {
            IdentityRole role = new IdentityRole(name);
            context.Roles.Add(role);

            var changes = await context.SaveChangesAsync();
            if (changes != 1)
            {
                logger.LogWarning($"Creating role '{name}' failed.");
            }
            else
            {
                logger.LogInformation($"Created role '{name}'");
            }

            return new Role() { ID = role.Id, Name = role.Name };
        }

        [HttpGet]
        public IEnumerable<Role> Read()
        {
            return context.Roles.Select(x => new Role() { ID = x.Id, Name = x.Name }).ToArray();
        }

        [HttpPost]
        public async Task UpdateAsync(string id, string name)
        {
            var updateRole = context.Roles.FirstOrDefault(x => x.Id == id);
            if (updateRole != null)
            {
                updateRole.Name = name;
                updateRole.NormalizedName = name.ToUpperInvariant();

                var changes = await context.SaveChangesAsync();
                if (changes != 1)
                {
                    logger.LogWarning($"Updating role '{name}' failed.");
                }
                else
                {
                    logger.LogInformation($"Updated role '{name}'");
                }
            }            
        }

        [HttpDelete]
        public async Task DeleteAsync(string id)
        {
            var deleteRole = context.Roles.FirstOrDefault(x => x.Id == id);
            if (deleteRole != null)
            {
                context.Roles.Remove(deleteRole);

                var changes = await context.SaveChangesAsync();
                if (changes != 1)
                {
                    logger.LogWarning($"Deleting role '{deleteRole.Name}' ({id}) failed.");
                }
                else
                {
                    logger.LogInformation($"Deleted role '{deleteRole.Name}' ({id})");
                }
            }
        }
    }
}
